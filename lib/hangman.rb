require 'byebug'

class Hangman
  attr_reader :guesser, :referee, :board

    def initialize players
      @guesser = players[:guesser]
      @referee = players[:referee]
    end

    def setup
      length = referee.pick_secret_word
      guesser.register_secret_length length, referee.name
      @board = Array.new length
    end

    def take_turn
      puts "Secret word: #{display_board.join(" ")}"
      guess = guesser.guess(@board)
      pos = referee.check_guess(guess)
      update_board(pos, guess)
      display_board
      pos.count > 0 ? puts("\"#{guess}\" is at position: #{(pos.map { |x| x + 1}).join(", ")}") : puts("\"#{guess}\" isn't in the word!")
      guesser.handle_response(guess, pos)
    end

    def update_board checks, guess
      checks.each { |check| @board[check] = guess }
    end

    def display_board
      @board.map { |letter| letter == nil ? "_" : letter }
    end

    def over? board
      @board.none? { |pos| !pos } || @guesser.stumped? || @guesser.got_it
    end

    def play
      setup
      play_count = 0
      until over? @board
        take_turn
        play_count += 1
        break if play_count == 26
      end
      if @guesser.stumped?
        puts "Secret word: #{display_board.join(" ")}"
        puts "#{@referee.name} has stumped #{@guesser.name}."
      elsif @guesser.got_it
        puts "Secret word: #{display_board.join(" ")}"
        puts "#{@guesser.name} got \"#{@guesser.got_it}\" in #{play_count} guesses!"
      else
        puts "All done! #{@guesser.name} guessed \"#{@board.join}\" in #{play_count} guesses!"
      end
    end
end

class HumanPlayer
  attr_reader :name

  def self.get_dict dictionary, name
    HumanPlayer.new(File.readlines(dictionary).map(&:chomp), name)
  end

  def initialize dictionary, name
    @dictionary = dictionary
    @name = name
  end

  def pick_secret_word
    print "Pick your secret word. Don't tell anyone!\nEnter the number of letters here: "
    gets.chomp.to_i
  end

  def check_guess letter
    print "#{@name}, is \"#{letter}\" in the word? (y/n) "
    case gets.chomp
    when "n"
      return []
    when "y"
      print "Which position(s) is the letter found at? (seperate with spaces) "
      poss = gets.chomp
      # poss_arr = poss.split.map { |pos| pos.to_i - 1} ###these will allow an error if you try to say a letter is somewher eyou already said
      # if (@board.map.with_index { |x,i| x == nil ? x : i }).compact
      poss.split.map { |pos| pos.to_i - 1}
    end
  end

  def register_secret_length length, opp
    puts "Got it, #{@name}? #{opp}'s word is #{length} letters long."
  end

  def guess board
    @guesses ||= []
    letter = ""
    while true
      print "Guess a letter: "
      letter = gets.chomp
      !(@guesses.include?(letter)) ? break : puts("You've already guessed that letter!")
    end
    @guesses << letter
    letter
  end

  def handle_response letter, poss
  end

  def stumped?
    false
  end

  def got_it #this could be added as an option later
    false
  end

end

class ComputerPlayer
  attr_reader :name

  def self.get_dict dictionary, name
    ComputerPlayer.new(File.readlines(dictionary).map(&:chomp), name)
  end

  def initialize dictionary, name
    @dictionary = dictionary
    @name = name
  end

  def pick_secret_word
    @secret_word = @dictionary.sample
    length = @secret_word.length
    puts "#{@name} chooses a word. It is #{length} letters long."
    length
  end

  def check_guess letter
    checks = []
    @secret_word.each_char.with_index { |char,i| checks << i if char == letter}
    checks
  end

  def register_secret_length length, opp
    @candidate_words = @dictionary.select { |word| word.length == length }
    @candidate_words.sort!
  end

  def guess board
    @guesses ||= []
    letters = Hash.new 0
    @candidate_words.join("").each_char { |char| letters[char] += 1 if !(board.include?(char) || @guesses.include?(char)) }
    letter = letters.key(letters.values.max)
    puts "#{@name} guesses \"#{letter}\"!"
    @guesses << letter
    letter
  end

  def handle_response letter, poss
    if poss.count != 0
      poss.each do |pos|
        @candidate_words.select! { |word| word[pos] == letter }
      end
      @candidate_words.select! do |word|
        word.each_char.with_index do |char,i|
          break false if char == letter && !(poss.include?(i))
        end
      end
    else
      @candidate_words.select! { |word| !(word.include?(letter)) }
    end
  end

  def stumped?
    @candidate_words.count == 0
  end

  def got_it
    return @candidate_words[0] if @candidate_words.count == 1
  end
  # an unused helper function
  # def word_places word
  #   @responses.each.with_index do |char,i|
  #     return false if char != (word[i] || nil)
  #   end
  # end
end

#Debug shit
# player1 = HumanPlayer.get_dict("dictionary.txt", "Nae")
# player2 = HumanPlayer.get_dict("dictionary.txt", "Ike")
# game = Hangman.new(guesser:player1, referee:player2)
if $PROGRAM_NAME == __FILE__

  print "Who is guessing? "
  name1 = gets.chomp
  print "Is #{name1} a computer? (y/n) "
  case gets.chomp
  when "y"
    player1 = ComputerPlayer.get_dict("dictionary.txt", name1)
  when "n"
    player1 = HumanPlayer.get_dict("dictionary.txt", name1)
  end
  print "And who is choosing the word? "
  name2 = gets.chomp
  print "Is #{name2} a computer? (y/n) "
  case gets.chomp
  when "y"
    player2 = ComputerPlayer.get_dict("dictionary.txt", name2)
  when "n"
    player2 = HumanPlayer.get_dict("dictionary.txt", name2)
  end

  game = Hangman.new(guesser:player1, referee:player2)
  game.play
end
